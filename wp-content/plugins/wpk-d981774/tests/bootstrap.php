<?php

require '../vendor/autoload.php';

function findWordpressBasePath() {
	$dir = dirname( __FILE__ );
	do {
		if ( file_exists( $dir . '/wp-config.php' ) ) {
			return $dir;
		}
	} while ( $dir = realpath( "$dir/.." ) );

	return null;
}

define( 'BASE_PATH', findWordpressBasePath() . '/' );
define( 'WP_USE_THEMES', false );

// phpcs:ignore
global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;

require BASE_PATH . 'wp-load.php';