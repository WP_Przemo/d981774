import Utils from './Utils';
import PhoneNumber from './PhoneNumbers';

let $ = jQuery.noConflict();

$( document ).on( 'ready', () => {

	let $form     = $( '.ipt_fsqm_main_form' ),
		instances = [];

	if ( $form.length ) {
		$form.each( function() {
			instances.push( new PhoneNumber( $( this ) ) );
		} );
	}

	Utils.addInstance( 'eform', instances );

} );
