import Utils from './Utils';
import Table from './Table';

let $ = jQuery.noConflict();

/**
 * Handles phone numbers dropdown in eForm
 *
 * */
class PhoneNumbers {

	constructor( $form ) {

		this.$form = $form;
		this.formID = this.getFormID();
		this.settings = this.fetchSettings();
		this.$loader = false;

		/**
		 * @property {Boolean} Deterimes if we are in middle of ajax call
		 * */
		this.isAjax = false;

		if ( this.settings ) {

			this.$input = this.getPhoneNumberInput();
			this.$dropdown = this.getDropdown();
			this.$state = this.getStateInput();
			this.$area = this.getAreaCodeInput();

			/**
			 * Stores phone number entered by user, on change we will fetch new numbers from api
			 *
			 * @property {Number}
			 * */
			this.number = this.$input.val();

			this.table = new Table( this.$dropdown.parents( '.ipt_uif_question' ) );

			this.$dropdown
				.find( 'option' )
				.remove()
				.end()
				.change();

			this
				.blockAreaTyping()
				.blockNumberTyping()
				.appendStates()
				.createLoader()
				.handleStepChange()
				.setupTableClick()
				.handleChange();

		}

	}

	/**
	 * Allow only max 3 characters in area input
	 *
	 * @return {PhoneNumbers}
	 * */
	blockAreaTyping() {

		this.$area.on( 'keyup keydown', function( e ) {

			let $this = $( this ),
				value = $this.val();

			if ( value.length > 3 ) {

				value = value.slice( 0, 3 );

				$this.val( value );

			}

		} );

		return this;

	}


	/**
	 * Allow only max 7 characters in area input
	 *
	 * @return {PhoneNumbers}
	 * */
	blockNumberTyping() {

		this.$input.on( 'keyup keydown', function( e ) {

			let $this = $( this ),
				value = $this.val();

			if ( value.length > 7 ) {

				value = value.slice( 0, 7 );

				$this.val( value );

			}

		} );

		return this;

	}

	/**
	 * Appends states to states dropdown
	 *
	 * @return {PhoneNumbers}
	 *
	 * */
	appendStates() {

		let $state = this.$state,
			states = PhoneNumbers.states;

		$state.html( '' ).append( '<option value="">Select state</option>' );

		for ( let state in states ) {

			if ( states.hasOwnProperty( state ) ) {

				let option = `<option value="${state}">${states[ state ]}</option>`;

				$state.append( option );

			}

		}

		return this;

	}

	/**
	 * @return void
	 * */
	showLoader() {
		this.$loader.addClass( 'wpk-active' );
	}

	/**
	 * @return void
	 * */
	hideLoader() {
		this.$loader.removeClass( 'wpk-active' );
	}

	/**
	 * Create loader element for our dropdown
	 *
	 * @return {PhoneNumbers}
	 *
	 * */
	createLoader() {

		let $loader       = $( '.ipt_uif_ajax_loader' ).eq( 0 ).clone(),
			$appendTarget = this.$dropdown.parent();

		//Remove text from loader
		$loader.addClass( 'wpk-loader' ).find( '.ipt-eform-preloader-text' ).remove();

		//Prepend loader to dropdown parent
		$appendTarget.addClass( 'wpk-relative' ).prepend( $loader );

		this.$loader = $loader;

		return this;

	}

	/**
	 * Get form ID from jQuery object
	 *
	 * @return {Number}
	 * */
	getFormID() {

		//id is stored in this format -> ipt_fsqm_form_53, we need to grab ID from it
		let elID = this.$form.attr( 'id' );

		return parseInt( elID.replace( 'ipt_fsqm_form_', '' ) );

	}

	/**
	 * Get form values
	 *
	 * @return {Object}
	 * */
	getValues() {
		return {
			state:    this.$state.val(),
			areaCode: this.$area.val(),
			number:   this.$input.val()
		};
	}

	/**
	 * Handle eform step change, if conditions are met fetch numbers from api
	 *
	 * @return {PhoneNumbers}
	 * */
	handleStepChange() {

		let Instance = this;

		this.$form.find( '.ipt_fsqm_form_button_next' ).on( 'click', function() {

			if ( Instance.$state.is( ':visible' ) && !Instance.$dropdown.is( ':visible' ) ) {

				let values = Instance.getValues();

				Instance.table.showLoader();

				Instance.fetchNumbers( values ).then( data => {
					Instance.populateTable( data.result );
				} );

			}

		} );

		return this;

	}

	/**
	 * Handle change events on form
	 *
	 * @return {PhoneNumbers}
	 * */
	handleChange() {

		const Instance = this,
			  handler  = () => {

				  if ( !this.$dropdown.is( ':visible' ) ) {
					  return;
				  }

				  let values = this.getValues();

				  this.table.showLoader();

				  this.fetchNumbers( values ).then( data => {
					  this.populateTable( data.result );
				  } );

			  };

		Utils.onFinishTyping( this.$input, handler );
		Utils.onFinishTyping( this.$area, handler );
		Utils.onFinishTyping( this.$input, handler );

		this.$state.on( 'change', handler );

		this.$dropdown.on( 'change', function() {

			$( this ).find( 'option:selected' ).each( function() {

				let rewrite = $( this ).val();

				Instance.table.$table
					.find( `tr[data-rewrite="${rewrite}"]` )
					.addClass( 'wpk-selected' )
					.find( '.wpk-add-number' )
					.addClass( 'wpk-in-cart' )
					.text( Utils.getMessage( 'added' ) );

			} );

		} );

		return this;

	}

	/**
	 * Fetch numbers from api, similar to provided one
	 *
	 * @return {Promise}
	 * */
	fetchNumbers( { areaCode, state, number } ) {

		return new Promise( resolve => {

			const Fd       = new FormData(),
				  Instance = this;

			Fd.append( 'number', number );
			Fd.append( 'state', state );
			Fd.append( 'areaCode', areaCode );
			Fd.append( 'action', 'wpk_fetch_numbers' );

			Utils.ajax( {
				data: Fd,
				beforeSend() {
					Instance.$dropdown.attr( 'disabled', true );

					Instance.isAjax = true;
				},
				success( data ) {
					resolve( data );
				},
				complete() {
					Instance.isAjax = false;

					Instance.$dropdown.attr( 'disabled', false );
				},

			} );

		} );

	}

	/**
	 * On table click append item to dropdown
	 *
	 * @return {PhoneNumbers}
	 *
	 * */
	setupTableClick() {

		const Instance = this;

		$( document ).on( 'click', '.wpk-table tbody .wpk-add-number', function() {

			let $this   = $( this ),
				$parent = $this.parents( 'tr' ),
				number  = $parent.data( 'number' ),
				rewrite = $parent.data('rewrite'),
				price   = $parent.data( 'price' ),
				option  = `<option selected data-num="${price}" value="${rewrite}">${number}</option>`;

			//Avoid duplicate options
			if ( Instance.$dropdown.find( `option[value="${rewrite}"]` ).length ) {
				return;
			}

			Instance.$dropdown
				.append( option )
				.change();

		} );

		$( document ).on( 'click', '.wpk-table tbody .wpk-in-cart', function() {

			let $this   = $( this ),
				$parent = $this.parents( 'tr' ),
				rewrite  = $parent.data( 'rewrite' ),
				option  = Instance.$dropdown.find( `option[value="${rewrite}"]` );

			//Remove class, so that the row will be refreshed while reloading table
			$parent.removeClass( 'wpk-selected' );

			option.remove();
			Instance.$dropdown.change();

			//Trigger table reload
			Instance.$state.change();

		} );

		return this;

	}

	/**
	 * Populate dropdown values with numbers from API
	 *
	 * @param {Boolean|Array} numbers
	 *
	 * @return void
	 * */
	populateTable( numbers ) {

		if ( numbers.length ) {

			numbers = numbers.map( item => {

				let state   = item.state,
					rewrite = item.rewrite.replace( '+', '' );

				//Format phone number
				let areaCode   = rewrite.substring( 1, 4 ),
					secondPart = rewrite.substring( 4, 7 ),
					lastPart   = rewrite.substring( 7, 11 );

				item.number = `(${areaCode}) ${secondPart}-${lastPart}`;
				item.state = PhoneNumbers.states[ state ];

				return item;

			} );

		}

		this.table.render( numbers ).hideLoader();

	}

	/**
	 * Fetch form settings
	 *
	 * @return {Object}
	 *
	 * */
	fetchSettings() {

		if ( !Array.isArray( Utils.vars.forms ) ) {
			return false;
		}

		let result = Utils.vars.forms.find( item => item.form_id == this.formID );

		if ( typeof result === 'object' ) {
			return result;
		}

		return false;

	}

	/**
	 * @return {jQuery}
	 * */
	getPhoneNumberInput() {

		let selector = `input[name*="[pinfo][${this.settings.phone_number}][value]"`;

		return this.$form.find( $( selector ) );

	}

	/**
	 * @return {jQuery}
	 * */
	getDropdown() {

		let selector = `select[name*="[mcq][${this.settings.phone_dropdown}][options]"`;

		return this.$form.find( $( selector ) );

	}

	/**
	 * @return {jQuery}
	 * */
	getStateInput() {

		let selector = `select[name*="[mcq][${this.settings.state_input}][options]"`;

		return this.$form.find( $( selector ) );

	}

	/**
	 * @return {jQuery}
	 * */
	getAreaCodeInput() {

		let selector = `input[name*="[pinfo][${this.settings.area_code_input}][value]"`;

		return this.$form.find( $( selector ) );

	}

}

PhoneNumbers.states = {
	'AL': 'Alabama',
	'AK': 'Alaska',
	'AS': 'American Samoa',
	'AZ': 'Arizona',
	'AR': 'Arkansas',
	'CA': 'California',
	'CO': 'Colorado',
	'CT': 'Connecticut',
	'DE': 'Delaware',
	'DC': 'District Of Columbia',
	'FM': 'Federated States Of Micronesia',
	'FL': 'Florida',
	'GA': 'Georgia',
	'GU': 'Guam',
	'HI': 'Hawaii',
	'ID': 'Idaho',
	'IL': 'Illinois',
	'IN': 'Indiana',
	'IA': 'Iowa',
	'KS': 'Kansas',
	'KY': 'Kentucky',
	'LA': 'Louisiana',
	'ME': 'Maine',
	'MH': 'Marshall Islands',
	'MD': 'Maryland',
	'MA': 'Massachusetts',
	'MI': 'Michigan',
	'MN': 'Minnesota',
	'MS': 'Mississippi',
	'MO': 'Missouri',
	'MT': 'Montana',
	'NE': 'Nebraska',
	'NV': 'Nevada',
	'NH': 'New Hampshire',
	'NJ': 'New Jersey',
	'NM': 'New Mexico',
	'NY': 'New York',
	'NC': 'North Carolina',
	'ND': 'North Dakota',
	'MP': 'Northern Mariana Islands',
	'OH': 'Ohio',
	'OK': 'Oklahoma',
	'OR': 'Oregon',
	'PW': 'Palau',
	'PA': 'Pennsylvania',
	'PR': 'Puerto Rico',
	'RI': 'Rhode Island',
	'SC': 'South Carolina',
	'SD': 'South Dakota',
	'TN': 'Tennessee',
	'TX': 'Texas',
	'UT': 'Utah',
	'VT': 'Vermont',
	'VI': 'Virgin Islands',
	'VA': 'Virginia',
	'WA': 'Washington',
	'WV': 'West Virginia',
	'WI': 'Wisconsin',
	'WY': 'Wyoming'
};


export default PhoneNumbers;