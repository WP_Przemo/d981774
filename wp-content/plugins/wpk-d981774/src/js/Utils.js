let $ = jQuery.noConflict();

class Utils {

	/**
	 * Check if value exists
	 *
	 * @param value Value to check
	 * @return {Boolean}
	 */
	static isset( value ) {

		return 'undefined' !== typeof value;

	}

	/**
	 *
	 * Add instance from job to global storage
	 *
	 * @param {String} key Instance key
	 * @param instance
	 *
	 * */
	static addInstance( key, instance ) {

		if ( !this.isset( window.wpk ) ) {
			window.wpk = {};
		}

		window.wpk[ key ] = instance;

	}

	/**
	 * Perform ajax call.
	 *
	 * @param {Object} args Additional settings for ajax
	 *
	 * */
	static ajax( args = [] ) {

		let settings = {
			type:        'POST',
			url:         Utils.vars.ajax_url,
			processData: false,
			contentType: false,
			error( xhr, ajaxOptions, thrownError ) {
				console.error( thrownError );
			},
		};

		settings = $.extend( settings, args );

		return $.ajax( settings );

	}

	/**
	 * Helper function for creating modal
	 *
	 * @param  {String} content Content of modal
	 * @param  {Boolean} fade Whenever modal should have fade in animation
	 * @param  {Object} css Additional css for modal
	 *
	 * @return {Promise}
	 */
	static modal( content, fade = false, css = {} ) {

		return new Promise( function( resolve, reject ) {

			let $container = $( '<div class="wpk-modal"></div>' ),
				overlay    = '<div class="wpk-modal-overlay"></div>',
				$body      = $( '.body' );


			$container.html( content );
			css = css || {};

			$body.append( $container );
			$body.append( overlay );

			let $modal      = $( '.wpk-modal' ),
				//Callback for removing modal
				removeModal = function() {
					$modal.fadeOut( 400, function() {
						$( this ).remove();
						$( '.wpk-modal-overlay' ).remove();
					} );
				};

			$modal.css( css );

			if ( fade ) {
				$modal.fadeIn( 400, function() {
					resolve( $modal );
				} );

			} else {
				$modal.show();
				resolve( $modal );
			}

			//Close modal when clicking overlay
			$( 'body, .wpk-modal-overlay' ).on( 'click', removeModal );

			$modal.on( 'click', function( e ) {
				e.stopPropagation();
			} );

		} );

	}

	/**
	 * Create event that launches when user finishes typing
	 *
	 * @return void
	 * */
	static onFinishTyping( $element, callback ) {

		let timeout;

		$element.on( 'keyup', function() {

			clearTimeout( timeout );

			timeout = setTimeout( () => callback.call( this ), 1000 );

		} ).on( 'keydown', function() {

			clearTimeout( timeout );

		} );

	}

	/**
	 * Get translated message from vars
	 *
	 * @param {String} message
	 *
	 * @return {String}
	 *
	 * */
	static getMessage( message ) {

		if ( !Utils.isset( Utils.vars.messages[ message ] ) ) {
			return '';
		}

		return Utils.vars.messages[ message ];

	}


}

Utils.vars = wpk_d981774_vars;

export default Utils;


