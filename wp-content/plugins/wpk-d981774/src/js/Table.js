import Utils from './Utils';
import 'datatables';
import 'datatables.net-bs4';

let $ = jQuery.noConflict();

/**
 * Handles creating and managing table with phone number
 *
 * @author Przemysław Żydek
 * */
class Table {

	constructor( $container ) {

		this.$container = $container;
		this.$table = false;
		this.$loader = false;

		this.createLoader().createTable();

	}

	/**
	 * Create loader element for our dropdown
	 *
	 * @return {Table}
	 *
	 * */
	createLoader() {

		let $loader = $( '.ipt_uif_ajax_loader' ).eq( 0 ).clone();

		//Remove text from loader
		$loader.addClass( 'wpk-loader wpk-active' ).find( '.ipt-eform-preloader-text' ).remove();

		this.$container.addClass( 'wpk-relative' ).prepend( $loader );

		this.$loader = $loader;

		return this;

	}

	/**
	 * @return {Table}
	 * */
	showLoader() {
		this.$loader.addClass( 'wpk-active' );

		return this;
	}

	/**
	 * @return {Table}
	 * */
	hideLoader() {
		this.$loader.removeClass( 'wpk-active' );

		return this;
	}


	/**
	 * Create table in DOM
	 *
	 * @return {Table}
	 * */
	createTable() {

		this.showLoader();

		this.$container.prepend( `
		<table class="wpk-table wpk-no-opacity">
			<thead>
				<tr>
					<td>${Utils.getMessage( 'phone_number' )}</td>
					<td>${Utils.getMessage( 'state' )}</td>
					<td>${Utils.getMessage( 'price' )}</td>
					<td></td>
				</tr>
			</thead>
			<tbody>
			
			</tbody>
		</table>
		` );

		this.$table = this.$container.find( '.wpk-table' );

		this.$container.find( '.ipt_uif_question_content, .ipt_uif_question_label' ).addClass( 'wpk-hidden' );

		this.getCount().then( count => {

			Table.settings = {
				'language':   {
					'emptyTable': Utils.getMessage( 'total' ).replace( '%s', count )
				},
				'columnDefs': [ {
					'targets':   3,
					'orderable': false
				} ]
			};

			this.$table.DataTable( Table.settings );

			this.hideLoader().$container.find( '.wpk-table' ).removeClass( 'wpk-no-opacity' );

		} );


		return this;

	}

	/**
	 *
	 * @return {Promise}
	 * */
	getCount() {

		let fd = new FormData();

		fd.append( 'action', 'wpk_get_count' );

		return new Promise( ( resolve, reject ) => {
			Utils.ajax( {
				data: fd,
				success( data ) {

					if ( data ) {
						resolve( data.result );
					} else {
						reject();
					}

				}
			} );
		} );

	}

	/**
	 * Render datatable
	 *
	 * @return {Table}
	 * */
	render( data ) {

		let $table = this.$table,
			table  = this.$table.DataTable();


		table.destroy();

		//Remove all number rows, except for these that are selected
		$table.find( 'tbody tr:not(.wpk-selected)' ).remove();

		if ( data.length ) {

			for ( let item of data ) {

				let { rewrite, state, price, number } = item;

				//Avoid displaying duplicates
				if ( $table.find( `tr[data-rewrite="${rewrite}"]` ).length ) {
					continue;
				}

				let html =
						`<tr data-price="${price}" data-number="${number}" data-rewrite="${rewrite}">
							<td>${number}</td>
							<td>${state}</td>
							<td>${price}</td>
							<td><span class="wpk-add-number ipt_uif_button eform-material-button">${Utils.getMessage( 'add_to_cart' )}</span></td>
						</tr>`;

				$table.find( 'tbody' ).append( html );

			}


		}

		$table.DataTable( Table.settings );

		return this;

	}

}

Table.settings = {};

export default Table;