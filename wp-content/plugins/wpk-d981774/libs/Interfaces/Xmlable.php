<?php


namespace Wpk\d981774\Interfaces;


interface Xmlable {

	/**
	 * @return \SimpleXMLElement
	 */
	public function toXML();

}