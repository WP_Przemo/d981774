<?php


namespace Wpk\d981774\Interfaces;

/**
 * Converts object to json
 */
interface Jsonable {

	/**
	 * @return string
	 */
	public function toJson();

}