<?php


namespace Wpk\d981774\Interfaces;

/**
 * Converts object to array
 */
interface Arrayable {

	/**
	 * @return array
	 */
	public function toArray();


}