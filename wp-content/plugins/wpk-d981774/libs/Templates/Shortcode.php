<?php


namespace Wpk\d981774\Templates;

/**
 * @author Przemysław Żydek
 */
abstract class Shortcode {

	const TAG = '';

	/**
	 * Shortcode constructor.
	 */
	public function __construct() {
		add_shortcode( self::TAG, [ $this, 'render' ] );
	}

	/**
	 * @param array $atts
	 *
	 * @return string
	 */
	abstract public function render( $atts = [] );

}