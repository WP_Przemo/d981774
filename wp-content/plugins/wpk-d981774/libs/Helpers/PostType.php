<?php


namespace Wpk\d981774\Helpers;

use Wpk\d981774\Utility;

/**
 * Helper function for creating new post types
 *
 * @author Przemysław Żydek
 */
class PostType {

	/** @var array Post type args */
	protected $args = [];

	/** @var string Post type slug */
	protected $slug = [];

	/**
	 * PostType constructor.
	 *
	 * @param string $slug
	 * @param array $args
	 */
	public function __construct( $slug, $args = [] ) {
		$this->slug = $slug;
		$this->args = $args;

		add_action( 'init', [ $this, 'register' ] );
	}

	/**
	 * Register post type. Hooked into init
	 *
	 * @return void
	 */
	public function register() {
		register_post_type( $this->slug, $this->args );
	}

	/**
	 * Add new column to admin view
	 *
	 * @param string $columnSlug
	 * @param string $columnName
	 * @param \Closure Callback for column content
	 *
	 * @return self
	 */
	public function addAdminColumn( $columnSlug, $columnName, \Closure $callback ) {

		add_filter( "manage_{$this->slug}_posts_columns", function ( $columns = [] ) use ( $columnSlug, $columnName ) {
			$columns[ $columnSlug ] = $columnName;

			return $columns;
		} );

		add_action( "manage_{$this->slug}_posts_custom_column", function ( $column, $postId ) use ( $callback, $columnSlug ) {
			if ( $column === $columnSlug ) {
				$callback( $postId );
			}
		}, 10, 2 );

		return $this;
	}

	/**
	 * @param string   $columnSlug
	 * @param string   $columnName
	 * @param array    $args Args for filtering (meta_key and orderby)
	 * @param \Closure $contentCallback
	 *
	 * @return $this
	 */
	public function addSortableColumn( $columnSlug, $columnName, $args = [], \Closure $contentCallback ) {

		$args = wp_parse_args( $args, [
			'meta_key' => $columnSlug,
			'orderby'  => 'meta_value_num',
		] );

		//Add column to view
		add_filter( "manage_{$this->slug}_posts_columns", function ( $columns = [] ) use ( $columnSlug, $columnName ) {
			$columns[ $columnSlug ] = $columnName;

			return $columns;
		} );

		//Make column sortable
		add_filter( "manage_edit-{$this->slug}_sortable_columns", function ( $columns = [] ) use ( $columnSlug, $columnName ) {
			$columns[ $columnSlug ] = $columnSlug;

			return $columns;
		} );

		//Callback for column content
		add_action( "manage_{$this->slug}_posts_custom_column", function ( $column, $postId ) use ( $contentCallback, $columnSlug ) {
			if ( $column === $columnSlug ) {
				$contentCallback( $postId );
			}
		}, 10, 2 );

		//Ordering
		add_action( 'pre_get_posts', function ( \WP_Query $query ) use ( $args ) {

			if ( ! is_admin() ) {
				return;
			}

			$queryOrder = $query->get( 'orderby' );

			if ( $queryOrder === $args[ 'meta_key' ] ) {

				foreach ( $args as $key => $item ) {
					$query->set( $key, $item );
				}

				$test = null;

			}

		} );

		return $this;

	}

	/**
	 * Remove admin column from table
	 *
	 * @param string $columnSlug
	 *
	 * @return $this
	 */
	public function removeAdminColumn( $columnSlug ) {

		add_filter( "manage_{$this->slug}_posts_columns", function ( $columns = [] ) use ( $columnSlug ) {
			unset( $columns[ $columnSlug ] );

			return $columns;
		} );

		return $this;

	}

	/**
	 * @param  string  $action
	 * @param  string  $label
	 * @param \Closure $callback Callback for this bulk action called with params $redirectTo and $postIDS
	 *
	 * @return self
	 */
	public function addBulkAction( $action, $label, \Closure $callback ) {

		add_filter( "bulk_actions-edit-{$this->slug}", function ( $actions ) use ( $action, $label ) {
			$actions[ $action ] = $label;

			return $actions;
		} );

		add_filter( "handle_bulk_actions-edit-{$this->slug}", function ( $redirectTo, $calledAction, $postIDS ) use ( $action, $callback ) {

			if ( $calledAction !== $action ) {
				return $redirectTo;
			}

			return $callback( $redirectTo, $postIDS );

		}, 10, 3 );

		return $this;

	}

	/**
	 * Add submenu to post page
	 *
	 * @param string $pageTitle
	 * @param string $menuTitle
	 * @param string $slug
	 *
	 * @return self
	 */
	public function addSubmenu( $pageTitle, $menuTitle, $slug ) {

		add_action( 'admin_menu', function () use ( $pageTitle, $menuTitle, $slug ) {
			add_submenu_page( "edit.php?post_type={$this->slug}", $pageTitle, $menuTitle, 'manage_options', $slug );
		} );

		return $this;

	}

	/**
	 * @param string $taxonomy
	 * @param array $args
	 *
	 * @return self
	 */
	public function addTaxonomy( $taxonomy, $args ) {

		$taxonomy = register_taxonomy( $taxonomy, $this->slug, $args );

		if ( is_wp_error( $taxonomy ) ) {
			Utility::log( $taxonomy->get_error_message() );
		}

		return $this;

	}

}