<?php


namespace Wpk\d981774\Api;

use GuzzleHttp\Exception\ServerException;
use Wpk\d981774\Settings;
use function Wpk\d981774\Core;
use Wpk\d981774\Guzzle\Response;
use Wpk\d981774\Helpers\Xml;
use Wpk\d981774\Models\Collection;
use Wpk\d981774\Models\Did;

/**
 * Client for backoffice.voipinnovations.com API
 *
 * @author Przemysław Żydek
 */
class Client {

	/** @var string */
	const ENDPOINT = 'https://backoffice.voipinnovations.com/Services/APIService.asmx';

	/** @var \Wpk\d981774\Guzzle\Client */
	protected static $client;

	/** @var Cache */
	protected static $cache;

	/**
	 * Loads API client
	 *
	 * @return void
	 */
	protected static function loadClient() {

		self::$client = new \Wpk\d981774\Guzzle\Client( [
				'verify' => false,
			]
		);

		self::$cache = new Cache( 'wpk_xml' );

	}

	/**
	 * Get initial XML object
	 *
	 * @return \SimpleXMLElement
	 */
	protected static function getXml() {

		$xml = <<<XML
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
				<soap:Body>
					
				</soap:Body>
			</soap:Envelope>
XML;

		return new \SimpleXMLElement( $xml );

	}

	/**
	 * Send post request to API
	 *
	 * @param string $endpoint
	 * @param array  $params
	 *
	 * @return Response
	 */
	public static function post( $endpoint, $params = [] ) {

		if ( empty( self::$client ) ) {
			self::loadClient();
		}

		$cache = self::$cache->get( $endpoint );

		if ( ! empty( $cache ) ) {
			return $cache;
		}

		$data = array_merge( $params, [
			'login'  => Settings::getSetting( 'api_login' ),
			'secret' => Settings::getSetting( 'api_password' ),
		] );

		//Key must be api endpoint:xml namespace
		$data = [
			"$endpoint:http://tempuri.org/" => $data,
		];

		$xml  = self::getXml();
		$body = $xml->children( 'soap', true );

		Xml::fromArray( $body, $data );

		try {
			$response = self::$client->post( self::ENDPOINT, [
				'body'    => $xml->asXML(),
				'headers' => [
					'Content-Type' => 'text/xml',
				],
			] );
		} catch ( ServerException $e ) {
			return new Response( $e->getResponse() );
		}

		if ( $response->getStatusCode() === 200 ) {
			self::$cache->add( $endpoint, $response );
		}

		return $response;


	}

	public static function getStates() {

		$response = Client::post( 'GetAvailableStates' );

		var_dump( $response->toXML()->saveXML( Core()->getPath( '/response.xml' ) ) );

	}

	/**
	 * @return Collection
	 */
	public static function getAuditDIDs() {

		$response = Client::post( 'auditDIDs' );


		$models = [];

		$response->toXML()->saveXML( Core()->getPath( '/response.xml' ) );

		if ( $response->getStatusCode() === 200 ) {

			$xml = $response->toXML()->children( 'soap', true );

			if ( $xml ) {
				//This is different respones code, provided by API
				if ( $xml->children()->auditDIDsResponse->auditDIDsResult->responseCode == 100 ) {
					foreach ( $xml->children()->auditDIDsResponse->auditDIDsResult->DIDs->children() as $did ) {
						$models[] = Did::create( $did );
					}
				}
			}

		}

		return Collection::make( $models );

	}

	/**
	 * @return void
	 */
	public static function clearCache() {

		if ( empty( self::$client ) ) {
			self::loadClient();
		}

		self::$cache->clear();
	}

}