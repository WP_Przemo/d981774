<?php


namespace Wpk\d981774\Api;

use Wpk\d981774\Guzzle\Response;

/**
 * Manages API cache
 */
class Cache {

	/** @var string Determines time in which value expires from cache */
	const EXPIRES = '+30 minutes';

	/** @var string */
	protected $optionSlug = '';

	/** @var array Stores cache data */
	protected $data = [];

	/**
	 * Cache constructor.
	 *
	 * @param string $optionSlug
	 */
	public function __construct( $optionSlug = 'wpk_xmls' ) {
		$this->optionSlug = $optionSlug;

		$this->getData();
	}

	/**
	 * Clear all cache data
	 *
	 * @return self
	 */
	public function clear() {

		$this->data = [];
		delete_option( $this->optionSlug );

		return $this;

	}

	/**
	 * Get data from cache
	 *
	 * @param string $endpoint
	 *
	 * @return bool|Response
	 */
	public function get( $endpoint ) {

		if ( ! isset( $this->data[ $endpoint ] ) ) {
			return false;
		}

		$data = $this->data[ $endpoint ];

		if ( (int) $data[ 'expires' ] < time() ) {
			unset( $data[ $endpoint ] );

			$this->updateData();

			return false;
		}

		return $data[ 'value' ];

	}

	/**
	 * Add endpoint value to cache
	 *
	 * @param string   $endpoint
	 * @param Response $value
	 */
	public function add( $endpoint, Response $value ) {

		$this->data[ $endpoint ] = [
			'value'   => $value,
			'expires' => strtotime( self::EXPIRES ),
		];

		$this->updateData();

	}

	/**
	 * Get data from DB
	 *
	 * @return self
	 */
	protected function getData() {
		$this->data = get_option( $this->optionSlug, [] );

		return $this;
	}

	/**
	 * @return self
	 */
	protected function updateData() {
		update_option( $this->optionSlug, $this->data );

		return $this;
	}

}