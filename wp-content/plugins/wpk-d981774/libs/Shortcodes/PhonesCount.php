<?php


namespace Wpk\d981774\Shortcodes;

use Wpk\d981774\Models\PhoneNumber;
use Wpk\d981774\Templates\Shortcode;

/**
 * @author Przemysław Żydek
 */
class PhonesCount extends Shortcode {

	const TAG = 'wpk_numbers_count';

	/**
	 * @param array $atts
	 *
	 * @return string
	 */
	public function render( $atts = [] ) {

		$count = PhoneNumber::init()->hasMetaValue( 'available', true )->perPage( - 1 )->get()->count();

		$message = sprintf( __( '<div class="wpk-numbers-count"><strong>%s</strong> numbers are waiting for you!</div>' ), $count );

		return $message;

	}
}