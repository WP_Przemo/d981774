<?php


namespace Wpk\d981774\Models;

use Wpk\d981774\Interfaces;

/**
 * @author Przemysław Żydek
 */
class Collection implements Interfaces\Jsonable, Interfaces\Arrayable {

	/** @var array */
	protected $items = [];

	/**
	 * Collection constructor.
	 *
	 * @param mixed $items
	 */
	public function __construct( $items = [] ) {
		$this->items = $this->getArrayItems( $items );
	}

	/**
	 * Parses items to array
	 *
	 * @param mixed $items
	 *
	 * @return array
	 */
	protected function getArrayItems( $items ) {
		return (array) $items;
	}

	/**
	 * @param mixed $items
	 *
	 * @return static
	 */
	public static function make( $items = [] ) {
		return new static( $items );
	}

	/**
	 * @return array
	 */
	public function all() {
		return $this->items;
	}

	/**
	 * @return int
	 */
	public function count() {
		return count( $this->items );
	}

	/**
	 * @return bool
	 */
	public function empty() {
		return empty( $this->items );
	}

	/**
	 * Check if items contain selected value by key
	 *
	 * @param mixed $key
	 * @param mixed $value
	 * @param bool  $strict
	 *
	 * @return bool
	 */
	public function contains( $key, $value, $strict = false ) {

		foreach ( $this->all() as $item ) {

			$item = (object) $item;

			if ( isset( $item->$key ) ) {

				if ( $strict && $item->$key === $value ) {
					return true;
				} else if ( $item->$key == $value ) {
					return true;
				}

			}
		}

		return false;

	}

	/**
	 * Find certain item by its values
	 *
	 * @param mixed $key
	 * @param mixed $value
	 * @param bool  $strict
	 *
	 * @return Collection
	 */
	public function find( $key, $value, $strict = false ) {

		$result = [];

		foreach ( $this->all() as $item ) {

			$item = (object) $item;

			if ( isset( $item->$key ) ) {

				if ( $strict && $item->$key === $value ) {
					$result[] = $item;
				} else if ( $item->$key == $value ) {
					$result[] = $item;
				}

			}
		}

		return new static( $result );

	}

	/**
	 * Execute a callback over each item.
	 *
	 * @param \Closure $callback
	 *
	 * @return $this
	 */
	public function each( \Closure $callback ) {

		foreach ( $this->items as $key => $item ) {
			if ( $callback( $item, $key ) === false ) {
				break;
			}
		}

		return $this;

	}

	/**
	 * Find value alike using similar_text
	 *
	 * @param mixed  $key
	 * @param string $value
	 * @param int    $similarity
	 *
	 * @return Collection
	 */
	public function like( $key, $value, $similarity = 8 ) {

		$result = [];

		foreach ( $this->all() as $item ) {
			if ( similar_text( $value, $item->$key ) >= $similarity ) {
				$result[] = $item;
			}
		}

		return new static( $result );

	}

	/**
	 * Get first item from collection
	 *
	 * @return bool|Entity
	 */
	public function first() {
		return isset( $this->items[ 0 ] ) ? $this->items[ 0 ] : false;
	}

	/**
	 * @return string
	 */
	public function toJson() {
		return (string) json_encode( $this->items );
	}

	/**
	 * @return array
	 */
	public function toArray() {
		return $this->all();
	}
}