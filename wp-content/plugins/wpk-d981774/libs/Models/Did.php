<?php


namespace Wpk\d981774\Models;

/**
 * Model for "DIDs"
 *
 * @author Przemysław Żydek
 */
class Did {

	/** @var array Array of availabilities that mean tha provided numer is available */
	CONST AVAILABILITIES = [ 'assigned' ];

	/** @var \SimpleXMLElement */
	protected $data;

	/** @var string Telephone number */
	public $tn;

	/** @var string */
	public $availability;

	/** @var string */
	public $rewrite;

	/** @var string */
	public $status;

	/** @var string */
	public $statusCode;

	/** @var string */
	public $state;

	/** @var string */
	public $cnamName;

	/** @var string */
	public $network;

	/** @var string */
	public $rateCenter;

	/** @var int */
	public $endpointId;

	/** @var int */
	public $areaCode;

	/** @var int Number without area code */
	public $number;

	public function __construct( \SimpleXMLElement $data ) {
		$this->data = $data;

		$this->parseData();
	}

	/**
	 * @return $this
	 */
	protected function parseData() {

		foreach ( (array) $this->data as $key => $item ) {
			if ( property_exists( $this, $key ) ) {
				$this->$key = $item;
			}
		}

		$this->getAreaCode();

		return $this;

	}

	/**
	 * Get area code from phone number
	 *
	 * @return $this
	 */
	protected function getAreaCode() {

		$split = str_split( $this->tn, 3 );

		$number = array_splice( $split, 1 );

		$this->areaCode = $split[ 0 ];
		$this->number   = implode( '', $number );

		return $this;

	}


	public function isAvailable() {
		return in_array( $this->availability, self::AVAILABILITIES );
	}

	/**
	 * @param \SimpleXMLElement $data
	 *
	 * @return static
	 */
	public static function create( \SimpleXMLElement $data ) {
		return new static( $data );
	}

}