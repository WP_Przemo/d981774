<?php

namespace Wpk\d981774\Guzzle;

/**
 * Wrapper for guzzle client
 *
 * @author Przemysław Żydek
 */
class Client extends \GuzzleHttp\Client {

	/**
	 * Client constructor.
	 *
	 * @param array $config
	 */
	public function __construct( $config = [] ) {

		if ( ! isset( $config[ 'verify' ] ) ) {
			$config[ 'verify' ] = false;
		}

		parent::__construct( $config );
	}

	/**
	 * @param \Psr\Http\Message\UriInterface|string $uri
	 * @param array                                 $options
	 *
	 * @return Response
	 */
	public function get( $uri, $options = [] ) {

		$res = parent::get( $uri, $options );

		return new Response( $res );

	}

	/**
	 * @param \Psr\Http\Message\UriInterface|string $uri
	 * @param array                                 $options
	 *
	 * @return \Psr\Http\Message\ResponseInterface|Response
	 */
	public function post( $uri, $options = [] ) {

		$res = parent::post( $uri, $options );;

		return new Response( $res );

	}

	/**
	 * Wrapper for sending XML in post request
	 *
	 * @param string            $uri
	 * @param \SimpleXMLElement $xml
	 * @param array             $headers
	 *
	 * @return Response
	 */
	public function postXml( $uri, \SimpleXMLElement $xml, $headers = [] ) {

		$headers = array_merge( $headers, [
			'Content-Type' => 'text/xml; charset=UTF8',
			'SOAPAction'   => 'http://tempuri.org/GetDIDGroup',
		] );

		$options = [
			'headers' => $headers,
			'body'    => $xml->asXML(),
		];

		$response = $this->get( $uri, $options );

		return $response;

	}

}