<?php


namespace Wpk\d981774\Guzzle;


use Psr\Http\Message\ResponseInterface;
use Wpk\d981774\Helpers\Xml;
use Wpk\d981774\Interfaces;
use Wpk\d981774\Utility;

class Response implements Interfaces\Arrayable, Interfaces\Jsonable, Interfaces\Xmlable {

	/** @var string Raw response */
	protected $response = '';

	/** @var int */
	protected $statusCode = 0;

	/** @var array */
	protected $headers = [];

	/**
	 * Response constructor.
	 *
	 * @param ResponseInterface $response
	 */
	public function __construct( ResponseInterface $response ) {

		$this->headers    = $response->getHeaders();
		$this->response   = (string) $response->getBody();
		$this->statusCode = $response->getStatusCode();

	}

	/**
	 * @return string
	 */
	public function __toString() {
		return (string) $this->response;
	}

	/**
	 * @return array
	 */
	public function toArray() {
		return json_decode( $this->response, true );
	}

	/**
	 * @return string
	 */
	public function toJson() {
		return $this->response;
	}

	/**
	 * Works only if response is an actual XML
	 *
	 * @param string $body Optional body tag, used only if response is not actualy XML and we have to convert it
	 *
	 * @return \SimpleXMLElement|false
	 */
	public function toXML( $body = '<body></body>' ) {

		if ( ! Utility::contains( $this->response, '<?xml' ) ) {

			$xml = new \SimpleXMLElement( $body );

			Xml::fromArray( $xml, $this->toArray() );

			return $xml;

		} else {

			return new \SimpleXMLElement( $this->response );

		}
	}

	/**
	 * @return int
	 */
	public function getStatusCode() {
		return $this->statusCode;
	}

	/**
	 * @return array|string
	 */
	public function getHeaders() {
		return $this->headers;
	}

}