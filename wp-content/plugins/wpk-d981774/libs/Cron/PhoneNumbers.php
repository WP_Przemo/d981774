<?php


namespace Wpk\d981774\Cron;

use Wpk\d981774\Api\Client;
use Wpk\d981774\Models\Did;
use Wpk\d981774\Models\PhoneNumber;
use Wpk\d981774\Settings;
use Wpk\d981774\Templates\Schedule;

/**
 * Cron task for fetching phone numbers from API
 *
 * @author Przemysław Żydek
 */
class PhoneNumbers extends Schedule {

	protected $recurrence = 'twicedaily';

	protected $callbacks = [ 'fetch' ];

	protected $hook = 'wpk_fetch_numbers';

	/**
	 * Fetch numbers from API and create posts if needed
	 *
	 * @return void
	 */
	public function fetch() {

		//Clear cache before API call
		Client::clearCache();

		$numbers = Client::getAuditDIDs();

		$defaultPrice = (float) Settings::getSetting( 'default_price', 200 );

		/** @var Did $number */
		foreach ( $numbers->all() as $number ) {

			$rewrite = $number->rewrite;

			$meta = get_object_vars( $number );

			$meta[ 'price' ] = $defaultPrice;

			//We can't serialize SimpleXMLElement class
			foreach ( $meta as $key => $item ) {
				if ( $item instanceof \SimpleXMLElement ) {
					unset( $meta[ $key ] );
				}
			}

			$meta[ 'formatted_number' ] = $this->formatNumber( $number->number );


			/** @var PhoneNumber $post */
			$post = PhoneNumber::init()->hasMetaValue( 'rewrite', $rewrite )->get()->first();

			if ( empty( $post ) ) {

				$meta[ 'available' ] = true;

				PhoneNumber::init()->title( $rewrite )->addMetas( $meta )->status( 'publish' )->create();

			} else {
				$post->updateMetas( $meta );
			}

		}

	}

	/**
	 * @param string|int $number
	 *
	 * @return string
	 */
	protected function formatNumber( $number ) {

		$number = (string) $number;

		$areaCode   = substr( $number, 0, 3 );
		$secondPart = substr( $number, 3, 3 );
		$lastPart   = substr( $number, 6, 4 );

		return "($areaCode) $secondPart-$lastPart";

	}

}