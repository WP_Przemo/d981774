<?php

namespace Wpk\d981774\Controllers;

use Wpk\d981774\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Middleware {

	use Request;

}