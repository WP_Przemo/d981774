<?php


namespace Wpk\d981774\Controllers\Eform;

use Wpk\d981774\Controllers\Controller;
use Wpk\d981774\Controllers\Eform\Middleware\Settings;
use Wpk\d981774\Models\PhoneNumber;

/**
 * @author Przemysław Żydek
 */
class Payment extends Controller {

	protected $middleware = [
		'settings' => Settings::class
	];

	/**
	 * Availability constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_filter( 'ipt_fsqm_filter_data_errors', [ $this, 'getPrices' ], 11, 2 );
		add_filter( 'ipt_fsqm_form_success_message', [ $this, 'validatePayment' ], 10, 4 );
		add_filter( 'ipt_fsqm_filter_payment_retry', [ $this, 'validatePaymentRetry' ], 10, 3 );

		add_action( 'ipt_fsqm_offline_payment_paid', [ $this, 'blockNumbers' ] );
	}

	/**
	 * Get numbers prices right before processing payment
	 *
	 * @param array $errors
	 * @param \IPT_FSQM_Form_Elements_Data $form
	 *
	 * @return array
	 */
	public function getPrices( $errors = [], \IPT_FSQM_Form_Elements_Data $form ) {

		$mcq        = $form->data->mcq;
		$dropdownID = $this->middleware( 'settings' )->getDropdownID( $form->form_id );

		if ( isset( $mcq[ $dropdownID ] ) ) {

			$fieldOptions = [];
			$numbers      = $mcq[ $dropdownID ][ 'options' ];

			foreach ( $numbers as $index => $number ) {

				/** @var PhoneNumber $numberPost */
				$numberPost = PhoneNumber::init()->hasMetaValue( 'rewrite', $number )->get()->first();

				if ( ! $numberPost ) {
					unset( $numbers[ $index ] );

					continue;
				}

				$fieldOptions[ $index ] = [
					'label' => $number,
					'score' => '',
					'num'   => $numberPost->meta( 'price' )
				];
			}

			$form->mcq[ $dropdownID ][ 'settings' ][ 'options' ] = $fieldOptions;

		}

		return $errors;

	}

	/**
	 * Validate form payment status. If payment succeded, block the phone number. Hooked into ipt_fsqm_form_success_message
	 * since only there we can access payment status
	 *
	 * @param array $successMessage
	 * @param array $paymentStatus
	 * @param array $wooData
	 * @param \IPT_FSQM_Form_Elements_Data $form
	 *
	 * @return array
	 */
	public function validatePayment( $successMessage = [], $paymentStatus = [], $wooData = [], \IPT_FSQM_Form_Elements_Data $form ) {

		if ( $paymentStatus[ 'success' ] && ! $paymentStatus[ 'needed' ] ) {

			$this->blockNumbers( $form );

		}

		return $successMessage;

	}

	/**
	 * Block phone numbers submitted in form, so that no one else can purschase them
	 *
	 * @param \IPT_FSQM_Form_Elements_Data $form
	 *
	 * @return void
	 */
	public function blockNumbers( \IPT_FSQM_Form_Elements_Data $form ) {

		$dropdownID = $this->middleware( 'settings' )->getDropdownID( $form->form_id );

		if ( ! empty( $dropdownID ) ) {

			$numbers = $form->data->mcq[ $dropdownID ][ 'options' ];

			foreach ( $numbers as $number ) {

				/** @var PhoneNumber $phone */
				$phone = PhoneNumber::init()->hasMetaValue( 'rewrite', $number )->hasMetaValue( 'available', true )->get()->first();

				if ( $phone ) {
					$phone->updateMetas( [
						'available'     => false,
						'submission_id' => $form->data_id
					] );
				}

			}

		}

	}

	/**
	 * Validate payment retry status. If payment succeded, block the phone number. Hooked into ipt_fsqm_form_success_message
	 * since only there we can access payment status
	 *
	 * @param array $message
	 * @param array $paymentStatus
	 * @param \IPT_FSQM_Form_Elements_Data $form
	 *
	 * @return array
	 */
	public function validatePaymentRetry( $message = [], $paymentStatus = [], \IPT_FSQM_Form_Elements_Data $form ) {

		if ( $paymentStatus[ 'success' ] && ! $paymentStatus[ 'needed' ] ) {
			$this->blockNumbers( $form );
		}

		return $message;

	}

}