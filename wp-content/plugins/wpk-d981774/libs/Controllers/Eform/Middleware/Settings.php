<?php


namespace Wpk\d981774\Controllers\Eform\Middleware;

use Wpk\d981774\Controllers\Middleware;
use Wpk\d981774\Models\Collection;

class Settings extends Middleware {

	/** @var Collection Stores eform related settings */
	protected $settings = [];

	/**
	 * Settings constructor.
	 */
	public function __construct() {

		add_action( 'init', function () {
			$this->settings = Collection::make( \Wpk\d981774\Settings::getSetting( 'fields' ) );
		} );

	}

	/**
	 * @param int $formID
	 * @param string $field
	 *
	 * @return bool|int
	 */
	protected function getFieldID( $formID, $field ) {
		$result = $this->settings->find( 'form_id', $formID )->first();

		return empty( $result ) ? false : (int) $result->$field;
	}

	/**
	 * @param $formID
	 *
	 * @return int
	 */
	public function getPhoneInputID( $formID ) {
		return (int) $this->getFieldID( $formID, 'phone_input' );
	}

	/**
	 * @param $formID
	 *
	 * @return int
	 */
	public function getDropdownID( $formID ) {
		return (int) $this->getFieldID( $formID, 'phone_dropdown' );
	}

	/**
	 * @param $formID
	 *
	 * @return int
	 */
	public function getStateInputID( $formID ) {
		return (int) $this->getFieldID( $formID, 'state_input' );
	}

	/**
	 * @param $formID
	 *
	 * @return int
	 */
	public function getAreaCodeInputID( $formID ) {
		return (int) $this->getFieldID( $formID, 'area_code_input' );
	}

}