<?php


namespace Wpk\d981774\Controllers\Eform;

use Wpk\d981774\Controllers\Controller;
use Wpk\d981774\Controllers\Eform\Middleware\Settings;
use Wpk\d981774\Models\PhoneNumber;
use Wpk\d981774\Utility;

/**
 * @author Przemysław Żydek
 */
class ErrorsHandler extends Controller {

	protected $middleware = [
		'settings' => Settings::class,
	];

	/**
	 * FormFields constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_filter( 'ipt_fsqm_filter_data_errors', [ $this, 'processErrors' ], 9, 2 );

	}

	/**
	 * @param array                        $errors
	 * @param \IPT_FSQM_Form_Elements_Data $form
	 *
	 * @return array
	 */
	public function processErrors( $errors = [], \IPT_FSQM_Form_Elements_Data $form ) {

		$formID = $form->form_id;

		$dropdownID = (int) $this->middleware( 'settings' )->getDropdownID( $formID );
		$stateID    = (int) $this->middleware( 'settings' )->getStateInputID( $formID );

		foreach ( $errors as $index => $error ) {

			$elementID = $error[ 'id' ];

			//We are looking for our dropdown, which is "mcq" element
			if ( Utility::contains( $elementID, 'mcq' ) ) {

				//Replace string, so that we are only left with actual element ID
				$elementID = (int) str_replace( "ipt_fsqm_form_{$formID}_mcq_", '', $elementID );

				if ( $elementID === $dropdownID ) {

					$numbers = $form->data->mcq[ $dropdownID ][ 'options' ];

					foreach ( $numbers as $number ) {

						//Let's check if number is still available
						$numberPost = PhoneNumber::init()->hasMetaRelation( [
							'rewrite'   => $number,
							'available' => true,
						] )->get()->first();

						if ( ! empty( $numberPost ) ) {
							unset( $errors[ $index ] );
						}

					}

				} else if ( $elementID === $stateID ) {

					unset( $errors[ $index ] );

				}

			}

		}

		return $errors;
	}

}