<?php


namespace Wpk\d981774\Controllers\Eform;

use Wpk\d981774\Api\Client;
use Wpk\d981774\Controllers\Controller;
use Wpk\d981774\Controllers\Eform\Middleware\Settings;
use Wpk\d981774\Helpers\Response;
use Wpk\d981774\Models\PhoneNumber;
use Wpk\d981774\Traits\Request;

/**
 * Handles populating eform dropdown values
 *
 * @author Przemysław Żydek
 */
class Populate extends Controller {

	/**
	 * Populate constructor.
	 */
	public function __construct() {
		parent::__construct();

		add_action( 'wp_ajax_nopriv_wpk_fetch_numbers', [ $this, 'fetchNumbers' ] );
		add_action( 'wp_ajax_wpk_fetch_numbers', [ $this, 'fetchNumbers' ] );

		add_action( 'wp_ajax_nopriv_wpk_get_count', [ $this, 'getCount' ] );
		add_action( 'wp_ajax_wpk_get_count', [ $this, 'getCount' ] );
	}

	/**
	 * @return void
	 */
	public function getCount() {

		$response = new Response();
		$phones   = PhoneNumber::init()->perPage( - 1 )->hasMetaValue( 'available', true )->get();

		$response->setResult( $phones->count() )->sendJson();

	}

	/**
	 * @return void
	 */
	public function fetchNumbers() {

		$response = new Response();

		$number   = $this->getPostParam( 'number' );
		$state    = $this->getPostParam( 'state' );
		$areaCode = $this->getPostParam( 'areaCode' );

		$phones = PhoneNumber::init();
		$metas  = [];

		if ( empty( $number ) && empty( $state ) && empty( $areaCode ) ) {
			$response->setResult( [] )->sendJson();
		}

		//Filter by state
		if ( ! empty( $state ) ) {
			$metas[ 'state' ] = $state;
		}

		if ( ! empty( $areaCode ) ) {
			$metas[ 'areaCode' ] = (int) $areaCode;
		}

		if ( ! empty( $number ) ) {
			$metas[ 'number' ] = [ $number, 'LIKE' ];
		}

		$metas[ 'available' ] = true;

		$phones->hasMetaRelation( $metas, 'AND' )->perPage( - 1 );

		$result = $phones->get();
		$dids   = [];

		foreach ( $result->all() as $item ) {
			/** @var PhoneNumber $item */

			$price = (float) $item->meta( 'price' );

			if ( empty( $price ) ) {
				$price = (float) \Wpk\d981774\Settings::getSetting( 'default_price', 200 );
			}

			$price = number_format( $price, 2, ',', '.' ) . '$';

			$dids[] = [
				'state'   => $item->meta( 'state' ),
				'rewrite' => $item->meta( 'rewrite' ),
				'price'   => $price,
			];
		}

		$response->setResult( $dids )->sendJson();

	}

}