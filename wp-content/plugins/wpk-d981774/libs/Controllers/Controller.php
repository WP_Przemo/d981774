<?php


namespace Wpk\d981774\Controllers;

use Wpk\d981774\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Controller {

	use Request;

	/**
	 * @var array Array with middleware classes to use
	 */
	protected $middleware = [];

	/**
	 * Controller constructor.
	 */
	public function __construct() {
		$this->loadMiddleware();
	}

	/**
	 * Perform load of middleware modules
	 *
	 * @return void
	 */
	protected function loadMiddleware() {

		foreach ( $this->middleware as $key => $middleware ) {
			$this->middleware[ $key ] = new $middleware();
		}

	}

	/**
	 * Get controller middleware
	 *
	 * @param string $middleware
	 *
	 * @return bool|mixed
	 */
	protected function middleware( $middleware ) {
		return isset( $this->middleware[ $middleware ] ) ? $this->middleware[ $middleware ] : false;
	}

}