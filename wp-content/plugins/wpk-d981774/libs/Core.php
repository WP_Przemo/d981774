<?php

namespace Wpk\d981774;

use Wpk\d981774\Controllers\Eform\ErrorsHandler;
use Wpk\d981774\Controllers\Eform\Payment;
use Wpk\d981774\Controllers\Eform\Populate;
use Wpk\d981774\Cron\PhoneNumbers;
use Wpk\d981774\Helpers\PostType;
use Wpk\d981774\Models\PhoneNumber;
use Wpk\d981774\Shortcodes\PhonesCount;

/**
 * Core class
 *
 * @author Przemysław Żydek
 */
final class Core {

	/** @var Core Stores class instance */
	private static $instance;

	/** @var array Shortcodes to load */
	private $shordcodes = [
		PhonesCount::class
	];

	/** @var string Slug used for translations */
	const SLUG = 'wpk-d981774';

	/** @var string */
	const NUMBERS_POST_TYPE = 'phone_number';

	/** @var string Stores path to this plugin directory */
	public $dir;

	/** @var string Stores url to this plugin directory */
	public $url;

	/** @var string Main plugin file */
	public $file;

	/** @var Loader Stores loader instance */
	public $loader;

	/** @var Utility */
	public $utility;

	/** @var Enqueue */
	public $enqueue;

	/** @var Settings */
	public $settings;

	/** @var array */
	public $controllers = [
		'eform' => [],
	];


	/**
	 * Core init
	 *
	 * @param string $file Main plugin file
	 * @param string $namespace Namespace of core class
	 *
	 * @return Core
	 */
	public function init( $file = null, $namespace = null ) {

		$this->file = empty( $file ) ? __FILE__ : $file;
		$this->url  = plugin_dir_url( $this->file );
		$this->dir  = plugin_dir_path( $this->file );

		$namespace = empty( $namespace ) ? __NAMESPACE__ : $namespace;

		$this->loader   = new Loader( $this->dir . '/libs', $namespace );
		$this->enqueue  = new Enqueue();
		$this->utility  = new Utility();
		$this->settings = new Settings();

		$this->eformControllers()->registerNumbersPostType()->loadShortcodes();

		return $this;

	}

	/**
	 * @return $this
	 */
	private function loadShortcodes() {
		foreach ( $this->shordcodes as $shortcode ) {
			new $shortcode();
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	private function registerNumbersPostType() {

		$postType = new PostType( self::NUMBERS_POST_TYPE, [
			'labels'             => [
				'name'               => _x( 'Phone numbers', 'post type general name', 'wpk' ),
				'singular_name'      => _x( 'Phone number ', 'post type singular name', 'wpk' ),
				'menu_name'          => _x( 'Phone numbers', 'admin menu', 'wpk' ),
				'name_admin_bar'     => _x( 'Phone number ', 'add new on admin bar', 'wpk' ),
				'add_new'            => _x( 'Add New', 'Phone number ', 'wpk' ),
				'add_new_item'       => __( 'Add New Phone number ', 'wpk' ),
				'new_item'           => __( 'New Phone number ', 'wpk' ),
				'edit_item'          => __( 'Edit Phone number ', 'wpk' ),
				'view_item'          => __( 'View Phone number ', 'wpk' ),
				'all_items'          => __( 'All Phone numbers', 'wpk' ),
				'search_items'       => __( 'Search Phone numbers', 'wpk' ),
				'parent_item_colon'  => __( 'Parent Phone numbers:', 'wpk' ),
				'not_found'          => __( 'No Phone numbers found.', 'wpk' ),
				'not_found_in_trash' => __( 'No Phone numbers found in Trash.', 'wpk' ),
			],
			'description'        => __( 'Phone numbers.', 'wpk' ),
			'public'             => true,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => [ 'slug' => self::NUMBERS_POST_TYPE ],
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'custom-fields' ],
		] );

		$postType
			->addAdminColumn( 'available', __( 'Available', 'wpk' ), function ( $postID ) {
				echo PhoneNumber::find( $postID )->meta( 'available' ) ? __( 'Yes', 'wpk' ) : __( 'No', 'wpk' );
			} )
			->addAdminColumn( 'state', __( 'State', 'wpk' ), function ( $postID ) {
				echo PhoneNumber::find( $postID )->meta( 'state' );
			} )
			->addAdminColumn( 'price', __( 'Price ($)', 'wpk' ), function ( $postID ) {
				echo PhoneNumber::find( $postID )->meta( 'price' );
			} )
			->removeAdminColumn( 'date' );

		//Load cron task
		new PhoneNumbers();

		return $this;

	}

	/**
	 * @return $this
	 */
	private function eformControllers() {

		$eform = &$this->controllers[ 'eform' ];

		$eform[ 'errors_handler' ] = new ErrorsHandler();
		$eform[ 'populate' ]       = new Populate();
		$eform[ 'payment' ]        = new Payment();

		return $this;

	}

	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public function getPath( $path ) {
		return "{$this->dir}/$path";
	}

	/**
	 * Get Core instance.
	 *
	 * @return Core
	 */
	public static function getInstance() {

		if ( empty( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;

	}
}
