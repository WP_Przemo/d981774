<?php
/*
Plugin Name: WP Kraken [#d981774]
Plugin URI: https://wpkraken.io/job/d981774
Description: eForm Custoisation API integration
Author: WP Kraken [Przemo]
Author URI: https://wpkraken.io/
Version: 1.0
Text Domain: wpk-d981774
*/

namespace Wpk\d981774;

//Require necessary files
require_once 'vendor/autoload.php';

require_once 'vendor/advanced-custom-fields-pro/acf.php';

require_once 'includes/wpk_functions.php';
require_once 'libs/Loader.php';
require_once 'libs/Core.php';


/**
 * Helper function for accessing Core.
 *
 * @return Core
 *
 * @author Przemysław Żydek
 */
function Core() {

	return Core::getInstance();

}

$core = Core();

/* Release the Kraken! */
$core->init( __FILE__ );