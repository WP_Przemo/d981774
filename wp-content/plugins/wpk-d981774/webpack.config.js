const path = require( 'path' );

module.exports = {
	mode:      'development',
	entry:     './src/js/main.js',
	output:    {
		filename: 'wpk-d981774.js',
		path:     path.resolve( __dirname, 'assets/js' )
	},
	module:    {
		rules: [
			{
				test:    /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use:     {
					loader:  'babel-loader',
					options: {
						presets: [ '@babel/preset-env' ],
					}
				}
			}
		]
	},
	externals: {
		jquery: 'jQuery'
	}
};